/*npm init - создает файл package.json с заисимостями и метаданным.
We use the npm install command to install new Node packages locally. The install command 
creates a folder called node_modules and copies 
the package files to it. The install command also installs all of the dependencies for the given package.
$ npm install babel-cli -D
$ npm install babel-preset-env -D. The -D flag instructs npm to add each package to a property called devDependencies 
in package.json. Once the project’s dependencies are listed in devDependencies, other developers can run your project without installing each package separately. Instead, they can simply run npm install — it instructs npm to look inside package.json and download all of the packages listed in devDependencies.
*/

/*Babel

после установки паккетов Babel нужно указать весрию исходного JS файла. Запускаем 
touch .babelrc
 в корне проекта
 в файле .babelrc создаем объект с ключом "presets:["env"]"
 env говорит о том, что будем транпсилировать код из ES6+*/

 /*Далеем идем в package.json --> scripts (объект с shortcut'ами ). Св-во "build":"babel src -d lib"
 babel — The Babel command call responsible for transpiling code.
src — Instructs Babel to transpile all JavaScript code inside the src directory.
-d — Instructs Babel to write the transpiled code to a directory.
lib — Babel writes the transpiled code to a directory called lib. */

/* вызываем команду npm run build --> в директории lib, создается тфайл с таким же навзанием с синтаксисом JS5*/

/*Используем CANIUSE.COM для проверки соместимости с браузером  */

var pasta = "Spaghetti"; // ES5 syntax

const meat = "Pancetta"; // ES6 syntax

let sauce = "Eggs and cheese"; // ES6 syntax

// Template literals, like the one below, were introduced in ES6
const carbonara = `You can make carbonara with ${pasta}, ${meat}, and a sauce made with ${sauce}.`;


/*MODULES */

// let Airplane = {};
// Airplane.myAirplane = "StarJet";
// module.exports = Airplane;



/*default export */


let Airplane = {};

Airplane.availableAirplanes = [
{
  name: 'AeroJet',
  fuelCapacity: 800
 }, 
 {name: 'SkyJet',
  fuelCapacity: 500
 }
];

export default Airplane;

//экспорт ф-ции
// export default function myFunctionName(parameters) {
//   //function body
// }



/*named export */

let availableAirplanes = [
  {
    name: 'AeroJet',
    fuelCapacity: 800,
    availableStaff: ['pilots', 'flightAttendants', 'engineers', 'medicalAssistance', 'sensorOperators']
   }, 
   {name: 'SkyJet',
    fuelCapacity: 500,
    availableStaff: ['pilots', 'flightAttendants']
   },
    
  ];
  
  let flightRequirements = {
    requiredStaff:4
  };
  function meetsStaffRequirements(availableStaff, requiredStaff) {
    if (availableStaff.length >= requiredStaff) {
      return true;
    } else {
      return false;
    }
  };
  
  export {availableAirplanes,flightRequirements,meetsStaffRequirements};