"use strict";

/*npm init - создает файл package.json с заисимостями и метаданным.
We use the npm install command to install new Node packages locally. The install command 
creates a folder called node_modules and copies 
the package files to it. The install command also installs all of the dependencies for the given package.
$ npm install babel-cli -D
$ npm install babel-preset-env -D. The -D flag instructs npm to add each package to a property called devDependencies 
in package.json. Once the project’s dependencies are listed in devDependencies, other developers can run your project without installing each package separately. Instead, they can simply run npm install — it instructs npm to look inside package.json and download all of the packages listed in devDependencies.
*/

/*Babel

после установки паккетов Babel нужно указать весрию исходного JS файла. Запускаем 
touch .babelrc
 в корне проекта
 в файле .babelrc создаем объект с ключом "presets:["env"]"
 env говорит о том, что будем транпсилировать код из ES6+*/

/*Далеем идем в package.json --> scripts (объект с shortcut'ами ). Св-во "build":"babel src -d lib"
babel — The Babel command call responsible for transpiling code.
src — Instructs Babel to transpile all JavaScript code inside the src directory.
-d — Instructs Babel to write the transpiled code to a directory.
lib — Babel writes the transpiled code to a directory called lib. */

var pasta = "Spaghetti"; // ES5 syntax

var meat = "Pancetta"; // ES6 syntax

var sauce = "Eggs and cheese"; // ES6 syntax

// Template literals, like the one below, were introduced in ES6
var carbonara = "You can make carbonara with " + pasta + ", " + meat + ", and a sauce made with " + sauce + ".";